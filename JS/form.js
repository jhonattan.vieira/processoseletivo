var formInput = document.querySelectorAll(".formBox input");
var formLabel = document.querySelectorAll(".formBox label");

for (let index = 0; index < formLabel.length; index++) {
    formInput[index].addEventListener("focus",function(){
        formInput[index].classList.add("formBorder");
        formLabel[index].classList.add("formColor");
    });

    formInput[index].addEventListener("blur",function(){
        formInput[index].classList.remove("formBorder");
        formLabel[index].classList.remove("formColor");
    });
}
